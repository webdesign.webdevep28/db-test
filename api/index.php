<?php
declare(strict_types=1);

require_once __DIR__ . '/../config.inc.php';

require_once __DIR__ . '/../services/LogService.class.php';
require_once __DIR__ . '/../services/ApiService.class.php';
require_once __DIR__ . '/../services/DatabaseService.class.php';

$dbOperator = new DatabaseService(DB_HOST, DB_USER, DB_PASS, DB_BASE);

switch (@$_SERVER['REQUEST_METHOD']) {
    case 'GET':
            $target = @$_GET['target'] ?? 'users';
            $targetId = intval(@$_GET['id'] ?? 0);

            echo json_encode(ApiService::get($target, $targetId, $dbOperator));

        break;

    case 'POST':
        try {
            $target = @$_POST['target'] ?? 'users';
            $targetId = intval(@$_POST['id'] ?? 0);

            unset($_POST['target']);
            unset($_POST['id']);
        } catch(\Exception $e ) {
            LogService::warning("Some troubles with \$_POST['target']");
        };

        // Update
        if ($targetId) {
            ApiService::update($target, $targetId, $_POST, $dbOperator);

            echo json_encode(['success' => true]);
        } else {
        // Create
            //
            $insertId = ApiService::insert($target, $_POST, $dbOperator);

            echo json_encode(['insertId' => $insertId]);
        };

        break;

    case 'DELETE':
        $target = @$_GET['target'] ?? 'users';
        $targetId = intval(@$_GET['id'] ?? 0);

        json_encode(ApiService::delete($target, $targetId, $dbOperator));

        echo json_encode(['success' => true]);

        break;
};