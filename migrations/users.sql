-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Июн 19 2022 г., 20:14
-- Версия сервера: 8.0.29-0ubuntu0.22.04.2
-- Версия PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dbtest`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `age` tinyint UNSIGNED NOT NULL,
  `firstname` varchar(127) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lastname` varchar(127) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `phone` varchar(32) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `age`, `firstname`, `lastname`, `phone`) VALUES
(29, 112, 'Вася пипкин112', 'qwe', 'aasds'),
(32, 112, 'Вася пипкин1', 'qwe', '221122'),
(43, 112, 'Вася пипкин1', 'qwe', 'qqq'),
(47, 112, 'Вася пипкин1', 'qwe', 'zzz123'),
(48, 33, '111sdf', '222', 'ыыы'),
(49, 33, 'assa', '222', '123'),
(58, 33, '111', '222', '1232'),
(61, 33, '111', '222', 'ууу123'),
(62, 33, '111', '222', 'ууу1234'),
(63, 33, '111', '222', 'ууу12345'),
(64, 33, '111', '222', 'ууу123456'),
(67, 33, '111', '222', 'ууу12345678'),
(70, 33, '111', '222', 'aaa'),
(75, 99, '123', '456', '789');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD KEY `age` (`age`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
