"use strict";

const api_link = location.origin + '/api/index.php';

var $obj = null;

var ready = (callback) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
};

ready(() => {
    console.log('Document started with ready callback.');

    GUIdrawUsersTable();

    /**
     * Mainmenu -> Insert
     */
    if ($obj = document.querySelector('#mainmenu [data-action="insert"]')) {
        $obj.addEventListener('click', function(e) {
            e.preventDefault();

            let insertUserForm = drawUserForm('insert');

            Swal.fire({
                title: `Insert a new user`,
                html: insertUserForm,
                showCancelButton: false,
                showConfirmButton: false,
                showCloseButton: true,
            });
        });
    };

    /**
     * Mainmenu -> Update
     */
    if ($obj = document.querySelector('#mainmenu [data-action="update"]')) {
        $obj.addEventListener('click', function(e) {
            e.preventDefault();

            let userData = {
                "id" : document.querySelector('#table-users tbody tr.active [data-name="id"]').innerText,
                "firstname" : document.querySelector('#table-users tbody tr.active [data-name="firstname"]').innerText,
                "lastname" : document.querySelector('#table-users tbody tr.active [data-name="lastname"]').innerText,
                "age" : document.querySelector('#table-users tbody tr.active [data-name="age"]').innerText,
                "phone" : document.querySelector('#table-users tbody tr.active [data-name="phone"]').innerText,
            };

            let updateUserForm = drawUserForm('update', userData);

            Swal.fire({
                title: `Update user ${userData.firstname} ${userData.lastname}`,
                html: updateUserForm,
                showCancelButton: false,
                showConfirmButton: false,
                showCloseButton: true,
            });
        });
    };

    /**
     * Table -> User item -> Double click
     */
    if ($obj = document.querySelector('#table-users')) {
        $obj.addEventListener('dblclick', function (e) {
            e.preventDefault();

            document.querySelector('#mainmenu [data-action="update"]').click();
        });
    };

    /**
     * Mainmenu -> Delete
     */
    if ($obj = document.querySelector('#mainmenu [data-action="delete"]')) {
        $obj.addEventListener('click', function(e) {
            e.preventDefault();

            let userData = {
                "id" : document.querySelector('#table-users tbody tr.active [data-name="id"]').innerText,
                "firstname" : document.querySelector('#table-users tbody tr.active [data-name="firstname"]').innerText,
                "lastname" : document.querySelector('#table-users tbody tr.active [data-name="lastname"]').innerText,
            };

            Swal.fire({
                title: `Action approve`,
                text: `Are you cure want to delete user ${userData.firstname} ${userData.lastname}?`,
                showCancelButton: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    fetch(api_link + '?id=' + userData.id, {
                        method: 'DELETE',
                        mode : 'cors',
                        cache : 'no-cache',
                        credentials : 'same-origin'
                    })
                    .then(response => response.json())
                    .then(response => {
                        if (response.error) {
                            console.error('Error:', response.error);
                        } else {
                            GUIdrawUsersTable();
                            GUIclosePopup();
                        };
                    })
                    .catch((error) => {
                        console.error('Error:', error);
                    });
                };
            });
        });
    };

    /**
     * Mainmenu -> Refresh
     */
    if ($obj = document.querySelector('#mainmenu [data-action="refresh"]')) {
        $obj.addEventListener('click', function(e) {
            e.preventDefault();

            GUIdrawUsersTable();
        });
    };
});

function insertOrUpdateData(packageData) {
    let data = new FormData();

    for (const prop in packageData) {
        data.set(prop, packageData[prop])
    };

    fetch(api_link, {
        method: 'POST',
        mode : 'cors',
        cache : 'no-cache',
        credentials : 'same-origin',
        body: data
    })
        .then(response => response.json())
        .then(response => {
            if (response.error) {
                console.error('Error:', response.error);
            } else {
                GUIdrawUsersTable();
                GUIclosePopup();
            };
        })
        .catch((error) => {
            console.error('Error:', error);

        });
}

function insertUser(obj) {
    let formId = obj.getAttribute('id');

    if (!formId) return null;

    let userData = {
        "firstname" : (document.querySelector(`#${formId} [name="firstname"]`) ? document.querySelector(`#${formId} [name="firstname"]`).value : '???'),
        "lastname" : (document.querySelector(`#${formId} [name="lastname"]`) ? document.querySelector(`#${formId} [name="lastname"]`).value : '???'),
        "age" : (document.querySelector(`#${formId} [name="age"]`) ? document.querySelector(`#${formId} [name="age"]`).value : '???'),
        "phone" : (document.querySelector(`#${formId} [name="phone"]`) ? document.querySelector(`#${formId} [name="phone"]`).value : '???'),
    };

    insertOrUpdateData(userData);
}

function updateUser(obj) {
    let formId = obj.getAttribute('id');

    if (!formId) return null;

    let userData = {
        "id" : (document.querySelector(`#${formId} [name="id"]`) ? document.querySelector(`#${formId} [name="id"]`).value : '0'),
        "firstname" : (document.querySelector(`#${formId} [name="firstname"]`) ? document.querySelector(`#${formId} [name="firstname"]`).value : '???'),
        "lastname" : (document.querySelector(`#${formId} [name="lastname"]`) ? document.querySelector(`#${formId} [name="lastname"]`).value : '???'),
        "age" : (document.querySelector(`#${formId} [name="age"]`) ? document.querySelector(`#${formId} [name="age"]`).value : '???'),
        "phone" : (document.querySelector(`#${formId} [name="phone"]`) ? document.querySelector(`#${formId} [name="phone"]`).value : '???'),
    };

    insertOrUpdateData(userData);
}

function GUIclosePopup() {
    if ($obj = document.querySelector('.swal2-container .swal2-close')) $obj.click();
}

async function GUIdrawUsersTable() {
    let $parentContainer = document.querySelector('#table-users tbody');

    $parentContainer.innerHTML = '';

    fetch(api_link, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(response => response.json())
        .then(users => {
            users.forEach((user) => {
                $parentContainer.innerHTML+= `
                    <tr onclick="setActiveTableRow(this)">
                        <td data-name="id">${user.id}</td>
                        <td data-name="firstname">${user.firstname}</td>
                        <td data-name="lastname">${user.lastname}</td>
                        <td data-name="age">${user.age}</td>
                        <td data-name="phone">${user.phone}</td>
                    </tr>
                `;
            })


        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

function setActiveTableRow(obj) {
    let otherRows = obj.parentElement.querySelectorAll('tr');

    otherRows.forEach((entry) => {
        entry.classList.remove('active');
    });

    obj.classList.add('active');

    unlockActionButtons();
}

function unlockActionButtons() {
    document.querySelector('#mainmenu [data-action="update"]').removeAttribute('disabled');
    document.querySelector('#mainmenu [data-action="delete"]').removeAttribute('disabled');
}

function drawUserForm(method, data = null) {
    let values = {
        "firstname": '',
        "lastname": '',
        "age": '',
        "phone": '',
    };

    if (data) {
        if (data.id) values.id = data.id;
        if (data.firstname) values.firstname = data.firstname;
        if (data.lastname) values.lastname = data.lastname;
        if (data.age) values.age = data.age;
        if (data.phone) values.phone = data.phone;
    };

    return `
    <form action="#" id="${method}UserForm" onsubmit = "event.preventDefault(); ${method}User(this)" class="container">
        <input type="hidden" name="id" value="${values.id}">
        <div class="row">
            <input type="text" name="firstname" value="${values.firstname}" placeholder="Firstname">
        </div>
        <div class="row">
           <input type="text" requried name="lastname" value="${values.lastname}" placeholder="Lastname">
        </div>
        <div class="row">
            <input type="number" required min="18" max="199" step="1" name="age" value="${values.age}" placeholder="Age">
        </div>
        <div class="row">
            <input type="text" required minlength="3" name="phone" value="${values.phone}" placeholder="Phone">
        </div>
        <div class="row">
            <div class="col-6"><button type="submit">Proceed</button></div>
            <div class="col-6"><button onclick = "document.querySelector('.swal2-close').click()" type="button" data-action="cancel">Cancel</button></div>                        
        </div>
    </form>`;
}