    <main class="col-12">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <nav id="mainmenu" class="btn-group me-2">
                <button type="button" data-action="insert" class="btn btn-sm btn-outline-secondary">Insert</button>
                <button type="button" data-action="update" class="btn btn-sm btn-outline-secondary" disabled="disabled">Update</button>
                <button type="button" data-action="delete" class="btn btn-sm btn-outline-secondary" disabled="disabled">Delete</button>
                <button type="button" data-action="refresh" class="btn btn-sm btn-outline-secondary">Refresh</button>
            </nav>
        </div>

        <h2>Users</h2>
        <div class="table-responsive">
            <table id = "table-users" class="data-table table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Firstname</th>
                    <th scope="col">Lastname</th>
                    <th scope="col">Age</th>
                    <th scope="col">Phone number</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                </tr>
                </tbody>
            </table>
        </div>
    </main>
</div>
