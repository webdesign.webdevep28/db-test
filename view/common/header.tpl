<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Igor Aleksandrov">
    <title>DB Test v 1.2</title>

    <style>
        * {
            outline: 0 !important;
            box-shadow: none !important;
            -moz-box-shadow: none !important;
            -o-box-shadow: none !important;
            -webkit-box-shadow: none !important;
        }
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
</head>
<body>

<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">DB Test v 1.2</a>
</header>

<div class="container-fluid">