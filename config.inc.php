<?php

define("ROOT_PATH", __DIR__ . DIRECTORY_SEPARATOR);

const EOL = '<br>';
const MIGRATIONS_PATH = 'migrations/';

const DB_HOST = 'localhost';
const DB_USER = 'root';
const DB_PASS = 'LurLMx7quC9nSNRC';
const DB_BASE = 'dbtest';

/*

First migration with creating table migrations:
 CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` date NOT NULL,
  `migrated_at` date DEFAULT NULL,
  `position` tinyint NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

***

Second migration with creating table users:

CREATE TABLE `users`.`users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `age` TINYINT UNSIGNED NOT NULL,
  `firstname` VARCHAR(127) NOT NULL,
  `lastname` VARCHAR(127) NOT NULL,
  `phone` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB;

ALTER TABLE `users` ADD PRIMARY KEY (`id`);
ALTER TABLE `users` MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `users` ADD UNIQUE(`phone`);
ALTER TABLE `users` ADD INDEX(`age`);

*/