<?php

require_once __DIR__ . '/../config.inc.php';

require_once __DIR__ . '/LogService.class.php';
require_once __DIR__ . '/contracts/ApiService.interface.php';
require_once __DIR__ . '/contracts/DatabaseService.interface.php';

class ApiService extends Singleton implements ApiServiceInterface
{
    /**
     * @param string $target
     * @param int $targetId
     * @param DatabaseServiceInterface $dbOperator
     * @return mixed
     */
    public static function get(string $target, int $targetId = 0, DatabaseServiceInterface $dbOperator)
    {
        return $targetId
            ? $dbOperator
                ->select()
                ->from($target)
                ->where('id', $targetId)
                ->run()
            : $dbOperator
                ->select()
                ->from($target)
                ->run();
    }

    /**
     * @param string $target
     * @param array $data
     * @param DatabaseServiceInterface $dbOperator
     * @return mixed
     */
    public static function insert(string $target, array $data, DatabaseServiceInterface $dbOperator)
    {
        return $dbOperator
            ->insert($data)
            ->into($target)
            ->run();
    }

    /**
     * @param string $target
     * @param int $targetId
     * @param array $data
     * @param DatabaseServiceInterface $dbOperator
     * @return mixed
     */
    public static function update(string $target, int $targetId, array $data, DatabaseServiceInterface $dbOperator)
    {
        return $dbOperator
            ->update($data)
            ->from($target)
            ->where('id', $targetId)
            ->run();
    }

    /**
     * @param string $target
     * @param $targetId
     * @param DatabaseServiceInterface $dbOperator
     * @return mixed
     */
    public static function delete(string $target, $targetId, DatabaseServiceInterface $dbOperator)
    {
        return $dbOperator
            ->delete()
            ->from($target)
            ->where('id', $targetId)
            ->run();
    }

    /**
     * @param string $target
     * @param string $key
     * @param string $value
     * @param $dbOperator
     * @return bool
     */
    public static function recordsCount(string $target, string $key, string $value, $dbOperator) : bool
    {
        $existRecords = $dbOperator
            ->select()
            ->from($target)
            ->where($key, $value)
            ->run();

        return count($existRecords) > 0  ? true : false;
    }
}