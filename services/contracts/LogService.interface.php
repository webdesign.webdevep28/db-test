<?php

interface LogServiceInterface
{
    public static function put(string $message, string $messageType, array $trace): void;

    public static function alert($message): void;

    public static function critical($message): void;

    public static function debug($message): void;

    public static function emergency($message): void;

    public static function error($message): void;

    public static function info($message): void;

    public static function notice($message): void;

    public static function warning($message): void;
}