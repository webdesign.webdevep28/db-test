<?php

interface ApiServiceInterface
{

    public static function get(string $target, int $id = 0, DatabaseServiceInterface $dbOperator);

    public static function insert(string $target, array $data, DatabaseServiceInterface $dbOperator);

    public static function update(string $target, int $targetId, array $data, DatabaseServiceInterface $dbOperator);

    public static function delete(string $target, $targetId, DatabaseServiceInterface $dbOperator);
}
