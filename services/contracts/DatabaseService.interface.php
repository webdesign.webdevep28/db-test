<?php

interface DatabaseServiceInterface
{
    public function __construct($host, $user, $pass, $dbname);

    public function init($host, $user, $pass, $dbname);

    public function select(?array $fields): DatabaseServiceInterface;

    public function truncate($table);

    public function count(): DatabaseServiceInterface;

    public function update(array $fields): DatabaseServiceInterface;

    public function delete(): DatabaseServiceInterface;

    public function from(string $table): DatabaseServiceInterface;

    public function into(string $table): DatabaseServiceInterface;

    public function where(string $field, $value, string $operator = '='): DatabaseServiceInterface;

    public function limit(int $start, int $offset): DatabaseServiceInterface;

    public function limitByCount(int $offset): DatabaseServiceInterface;

    public function order(string $field, string $direction): DatabaseServiceInterface;

    public function getQuery(): string;

    public function runQuery(string $query);

    public function getResult();

    public function getInsertId(): int;

    public function prepare();

    public function execute(bool $sqlIsPrepared = false);

    /* Synonym functions */

    public function orderBy(string $field, string $direction): DatabaseServiceInterface;

    public function fromTable(string $table): DatabaseServiceInterface;

    public function getSql(): string;

    public function run();
}