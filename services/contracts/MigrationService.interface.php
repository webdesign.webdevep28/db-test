<?php

interface MigrationServiceInterface
{
    public function __construct(DatabaseServiceInterface $databaseOperator);

    public function make($direction, $migrationName): bool;

    public function makeAll($direction);

    public function printAll();

    public function create($comment = ''): ?string;

    public function getNewMigrations($direction = 'up'): ?array;

    public function getAllMigrations(): ?array;

    public function getMigrationPosition($migrationName);

    public function insertNewMigrationIntoDB($name, $created_at, $migrated_at, $comment = '');

    public function refresh();
}