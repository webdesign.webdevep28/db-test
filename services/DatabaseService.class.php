<?php

require_once __DIR__ . '/core/Singleton.pattern.php';
require_once __DIR__ . '/contracts/DatabaseService.interface.php';

class DatabaseService implements DatabaseServiceInterface
{
    protected stdClass $query;
    protected $mysqli;

    /**
     * @return void
     */
    protected function reset(): void
    {
        $this->query = new \stdClass();
    }

    /**
     * @param $str
     * @return string
     */
    protected function escape_string($str): string
    {
        // Shield in one of the ways
        return htmlentities($str);
    }

    /**
     * @param $str
     * @return string
     */
    protected function unescape_string($str): string
    {
        // Unshield in one of the ways
        return html_entity_decode($str);
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function buildQuery(): string
    {
        if ($this->query->type == 'query') {
            return $this->sql;
        } else {
            if (!@$this->query->table) {
                throw new \Exception("You forget to set `from` parameter in your SQL query");
            };

            $query = $this->query;
            $sql = $this->query->base;

            if (in_array($this->query->type, ['select', 'count', 'update', 'delete'])) {
                $sql .= " WHERE " . (empty($query->where) ? '1' : implode(' AND ', $query->where));
            };

            if (isset($query->order)) {
                $sql .= $query->order;
            };

            if (isset($query->limit)) {
                $sql .= $query->limit;
            };

            $sql .= ";";

            return $this->sql = $sql;
        };
    }

    /**
     * @param $host
     * @param $user
     * @param $pass
     * @param $dbname
     */
    public function __construct($host, $user, $pass, $dbname)
    {
        $this->init($host, $user, $pass, $dbname);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->sql;
    }

    /**
     * @param $host
     * @param $user
     * @param $pass
     * @param $dbname
     * @return void
     * @throws Exception
     */
    public function init($host, $user, $pass, $dbname)
    {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

        $this->mysqli = new mysqli($host, $user, $pass, $dbname);

        if ($this->mysqli->connect_error) {
            throw new \Exception('MYSQLI Connect Error (' . $this->mysqli->connect_errno . ') ' . $this->mysqli->connect_error);
        };
    }

    /**
     * @param array|null $fields
     * @return DatabaseServiceInterface
     */
    public function select(?array $fields = []): DatabaseServiceInterface
    {
        $this->reset();
        $this->query->type = 'select';

        foreach ($fields as &$field) {
            $field = "`" . $this->escape_string($field) . "`";
        };

        $this->query->base = "SELECT " . (empty($fields) ? '*' : implode(", ", $fields)) . " FROM `{{TABLE}}`";

        return $this;
    }

    /**
     * @param $table
     * @return DatabaseServiceInterface
     */
    public function truncate($table)
    {
        $this->reset();
        $this->query->type = 'truncate';
        $this->query->table = $table;
        $this->query->base = "TRUNCATE TABLE `$table`";

        return $this->execute();
    }

    /**
     * @return DatabaseServiceInterface
     */
    public function count(): DatabaseServiceInterface
    {
        $this->reset();
        $this->query->type = 'count';
        $this->query->base = "SELECT COUNT(*) AS `count` FROM `{{TABLE}}`";

        return $this;
    }

    /**
     * @param array $fields
     * @return DatabaseServiceInterface
     */
    public function update(array $fields): DatabaseServiceInterface
    {
        $this->reset();
        $this->query->type = 'update';
        $this->query->base = "UPDATE IGNORE `{{TABLE}}` SET ";
        $couples = array();

        foreach ($fields as $key => $value) {
            $couples[] = "`" . $this->escape_string($key) . "` = '" . $this->escape_string($value) . "'";
        };

        $this->query->base .= implode(',', $couples);

        return $this;
    }

    /**
     * @param array $fields
     * @return DatabaseServiceInterface
     */
    public function insert(array $fields): DatabaseServiceInterface
    {
        $this->reset();
        $this->query->type = 'insert';
        $keys = array();
        $values = array();
        $couples = array();

        foreach ($fields as $key => $value) {
            $couples[] = "`" . $this->escape_string($key) . "` = '" . addslashes($this->escape_string($value)) . "'";
            $keys[] = "`" . $this->escape_string($key) . "`";
            $values[] = "'" . addslashes($this->escape_string($value)) . "'";
        };

        $this->query->base = "INSERT INTO `{{TABLE}}` (" . implode(',', $keys) . ") VALUES (" . implode(',', $values) . ") ON DUPLICATE KEY UPDATE " . implode(',', $couples);

        return $this;
    }

    /**
     * @return DatabaseServiceInterface
     */
    public function delete(): DatabaseServiceInterface
    {
        $this->reset();
        $this->query->type = 'delete';
        $this->query->base = "DELETE FROM `{{TABLE}}`";

        return $this;
    }

    /**
     * @param string $table
     * @return DatabaseServiceInterface
     */
    public function from(string $table): DatabaseServiceInterface
    {
        $this->query->base = str_replace('{{TABLE}}', $table, $this->query->base);
        $this->query->table = $table;

        return $this;
    }

    /**
     * @param string $table
     * @return DatabaseServiceInterface
     */
    public function fromTable(string $table): DatabaseServiceInterface
    {
        return $this->from($table);
    }

    /**
     * @param string $table
     * @return DatabaseServiceInterface
     */
    public function into(string $table): DatabaseServiceInterface
    {
        return $this->from($table);
    }

    /**
     * @param string $field
     * @param $value
     * @param string $operator
     * @return DatabaseServiceInterface
     * @throws Exception
     */
    public function where(string $field, $value, string $operator = '='): DatabaseServiceInterface
    {
        if (!in_array($this->query->type, ['select', 'count', 'update', 'delete'])) {
            throw new \Exception("WHERE clause can only be added to SELECT, UPDATE OR DELETE");
        };

        if ($value == 'NULL')
            $this->query->where[] = "`{$field}` {$operator} {$value}";
        else
            $this->query->where[] = "`{$field}` {$operator} '{$value}'";

        return $this;
    }

    /**
     * @param int $start
     * @param int $offset
     * @return DatabaseServiceInterface
     * @throws Exception
     */
    public function limit(int $start, int $offset): DatabaseServiceInterface
    {
        if (!in_array($this->query->type, ['select'])) {
            throw new \Exception("LIMIT can only be added to SELECT");
        };

        $this->query->limit = " LIMIT {$start}, {$offset}";

        return $this;
    }

    /**
     * @param int $offset
     * @return DatabaseServiceInterface
     */
    public function limitByCount(int $offset): DatabaseServiceInterface
    {
        if (!in_array($this->query->type, ['select'])) {
            throw new \Exception("LIMIT can only be added to SELECT");
        };

        $this->query->limit = " LIMIT {$offset}";

        return $this;
    }

    /**
     * @param string $field
     * @param string $direction
     * @return DatabaseServiceInterface
     * @throws Exception
     */
    public function order(string $field, string $direction = 'ASC'): DatabaseServiceInterface
    {
        if (!in_array($this->query->type, ['select'])) {
            throw new \Exception("ORDER can only be added to SELECT");
        };

        $this->query->order = " ORDER BY `{$field}` " . $direction;

        return $this;
    }

    /**
     * @param string $field
     * @param string $direction
     * @return DatabaseServiceInterface
     * @throws Exception
     */
    public function orderBy(string $field, string $direction = 'ASC'): DatabaseServiceInterface
    {
        return $this->order($field, $direction);
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return $this->sql;
    }

    /**
     * @return string
     */
    public function getSql(): string
    {
        return $this->sql;
    }

    /**
     * @param string $sql
     * @return array|int|void|null
     * @throws Exception
     */
    public function runQuery(string $sql)
    {
        $this->reset();
        $this->query->type = 'query';
        $this->sql = $sql;

        return $this->execute();
    }

    /**
     * @return null
     */
    public function getResult()
    {
        return $this->result ?? null;
    }

    /**
     * @return int
     */
    public function getInsertId(): int
    {
        return $this->mysqli->insert_id ?? 0;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function prepare()
    {
        $this->buildQuery();

        return $this;
    }

    /**
     * @param $sqlIsPrepared
     * @return array|int|void|null
     * @throws Exception
     */
    public function execute($sqlIsPrepared = false)
    {
        if (!$sqlIsPrepared)
            $this->prepare();

        if (!$this->sql) {
            throw new \Exception("Nothing to run: SQL is null");
        };

//        LogService::debug($this->sql);

        if ($result = $this->mysqli->query($this->sql, MYSQLI_USE_RESULT)) {
            switch ($this->query->type) {
                case 'count':
                    $this->result = array();

                    if (is_object($result)) {
                        while ($row = $result->fetch_assoc()) {
                            $this->result = intval($row['count']);
                        };
                    }

                    return $this->result;

                case 'insert':
                    return $this->getInsertId();

                default:
                    $this->result = array();

                    if (is_object($result)) {
                        while ($row = $result->fetch_assoc()) {
                            $this->result[] = $row;
                        };
                    };

                    return $this->result;
            };
        } else {
            // Do not throw the fatal, just write to error.log about situation

            LogService::error("DB query failed. Check your SQL syntax");
        };

        $this->sql = '';
    }

    /**
     * @return array|int|void|null
     * @throws Exception
     */
    public function run()
    {
        return $this->execute();
    }
}