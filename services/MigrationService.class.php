<?php

require_once __DIR__ . '/../config.inc.php';
require_once __DIR__ . '/contracts/DatabaseService.interface.php';
require_once __DIR__ . '/contracts/MigrationService.interface.php';
require_once __DIR__ . '/LogService.class.php';

class MigrationService implements MigrationServiceInterface
{

    protected DatabaseServiceInterface $dbo;

    /**
     * @param DatabaseServiceInterface $databaseOperator
     */
    public function __construct(DatabaseServiceInterface $databaseOperator)
    {
        $this->dbo = $databaseOperator;
    }

    /* Protected methods */

    /**
     * @param $created_at
     * @param $position
     * @return string
     */
    protected function getMigrationName($created_at, $position): string
    {
        $convertedPosition = $position;
        $convertedCreatedAt = str_ireplace('-', '', $created_at);

        if (((int)$position) < 10) $convertedPosition = "0{$position}";

        return "{$convertedCreatedAt}{$convertedPosition}";
    }

    /* Public methods */

    /**
     * @param $direction
     * @param $migrationName
     * @return bool
     */
    public function make($direction, $migrationName): bool
    {
        $sql = '';

        try {
            echo "\r\nExecuting " . ROOT_PATH . MIGRATIONS_PATH . "{$migrationName}.migration.php";

            if ($direction == 'up' || $direction == 'down') {

                require ROOT_PATH . MIGRATIONS_PATH . "{$migrationName}.migration.php";

                try {
                    $className = "Migration_{$migrationName}";

                    $migration = new $className($this->dbo);

                    if ($migration) {
                        if ($direction == 'down') {
                            $migration->down();

                            $this->dbo
                                ->update([
                                    'migrated_at'   => NULL
                                ])
                                ->from('migrations')
                                ->where('name', $migrationName)
                                ->run();

                            return true;
                        } else {
                            $migration->up();

                            $this->dbo
                                ->update([
                                    'migrated_at'   => date('Y-m-d')
                                ])
                                ->from('migrations')
                                ->where('name', $migrationName)
                                ->run();

                            return true;
                        };
                    } else {
                        echo "\r\nClass {$className} does not exists.";

                        return false;
                    }
                } catch (Exception $e) {
                    echo "\r\nCan not create an instance of migration class. Check if it exists in file {$migrationName}.migration.php";

                    return false;
                }
            } else {
                echo "\r\nYou should choose direction of migration first (up|down).";

                return false;
            }
        } catch (Exception $e) {
            echo "\r\nException: {$e->getMessage()}";

            return false;
        };

        return true;
    }

    /**
     * @return void
     */
    public function makeAll($direction)
    {
        $migrations = $this->getNewMigrations($direction);

        if (empty($migrations)) {
            echo "\r\n\r\nNothing to migrate.";
        } else {
            foreach ($migrations as $migration) {
                try {
                    $migrationName = $this->getMigrationName($migration['created_at'], $migration['position']);

                    $this->make($direction, $migration['name']);

                    echo "\r\nMigration {$migrationName} is completed: {$direction}.";
                } catch (Exception $e) {
                    echo "\r\nException: {$e->getMessage()}";
                };
            };
        }
    }

    /**
     * @return void
     */
    public function printAll()
    {
        $migrations = $this->dbo
            ->select()
            ->from('migrations')
            ->run();

        if (empty($migrations)) {
            echo "\r\n\r\nNo migrations yet.";
        } else {
            $mask = "|%2s |%15s |%12s |%30s\n";

            printf($mask, 'ID', 'Migration', 'Migrated', 'Comment');
            echo "\r\n";

            foreach ($migrations as $migration) {
                $migrationName = $this->getMigrationName($migration['created_at'], $migration['position']);

                printf($mask, $migration['id'], $migrationName, ($migration['migrated_at'] ? $migration['migrated_at'] : '—'), $migration['comment']);
            };
        }
    }

    /**
     * @param $comment
     * @return str|null
     */
    public function create($comment = ''): ?string
    {
        if (!file_exists(ROOT_PATH . MIGRATIONS_PATH)) {
            mkdir(ROOT_PATH . MIGRATIONS_PATH);
        };

        try {
            $position = $this->dbo->count()->from('migrations')->where('created_at', date('Y-m-d'))->run() + 1;
            $migrationName = $this->getMigrationName(date('Y-m-d'), $position);
            $migrationFilename = ROOT_PATH . MIGRATIONS_PATH . "{$migrationName}.migration.php";

            $f = fopen($migrationFilename, 'a+');

            $migration_comment = " 
/**
 * Migration {$migrationName}
 * {$comment}
 */
 
";
            fwrite($f, "<?php{$migration_comment}class Migration_{$migrationName}\r\n{\r\n\r\n\tprotected DatabaseServiceInterface \$dbo;\r\n\r\n\tpublic function __construct(DatabaseServiceInterface \$databaseOperator)\r\n\t{\r\n\t\t\$this->dbo = \$databaseOperator;\r\n\t}\r\n\r\n\tpublic function up()\r\n\t{\r\n\t\t\$sql_array = array(\r\n\r\n\t\t);\r\n\r\n\t\ttry {\r\n\t\t\tforeach (\$sql_array as \$sql) {\r\n\t\t\t\t\$this->dbo->runQuery(\$sql)->run();\r\n\t\t\t}\r\n\t\t} catch (Exception \$e) {\r\n\t\t\tthrow new \Exception(\$e->getMessage());\r\n\t\t};\r\n\t}\r\n\r\n\tpublic function down()\r\n\t{\r\n\t\t\$sql_array = array(\r\n\r\n\t\t);\r\n\r\n\t\ttry {\r\n\t\t\tforeach (\$sql_array as \$sql) {\r\n\t\t\t\t\$this->dbo->runQuery(\$sql)->run();\r\n\t\t\t}\r\n\t\t} catch (Exception \$e) {\r\n\t\t\tthrow new \Exception(\$e->getMessage());\r\n\t\t};\r\n\t}\r\n}");
            fclose($f);

            $this->dbo
                ->insert([
                    'created_at'    => date('Y-m-d'),
                    'position'      => $position,
                    'name'          => $migrationName,
                    'comment'       => $comment
                ])
                ->into('migrations')
                ->run();

//            $sql = "INSERT INTO `migrations` (`created_at`, `position`, `name`, `comment`) VALUES ('" . date('Y-m-d') . "', '{$position}', '{$migrationName}', '{$comment}') ON DUPLICATE KEY UPDATE `created_at` = '" . date('Y-m-d') . "', `migrated_at` = NULL";
//            $this->modx->query($sql);

            return $migrationName;
        } catch (Exception $e) {
            throw new \Exception($e->getMessage());

            return null;
        };

        return true;
    }

    /**
     * @return array|null
     */
    public function getNewMigrations($direction = 'up'): ?array
    {
        if ($direction == 'up')
            return $this->dbo
                ->select(['name', 'created_at', 'position'])
                ->from('migrations')
                ->where('migrated_at', 'NULL', 'IS')
                ->run();
        elseif ($direction == 'down')
            return $this->dbo
                ->select(['name', 'created_at', 'position'])
                ->from('migrations')
                ->where('migrated_at', 'NULL', 'IS NOT')
                ->run();

        return [];
    }

    /**
     * @return array|null
     */
    public function getAllMigrations(): ?array
    {
        return $this->dbo
            ->select()
            ->from('migrations')
            ->run();
    }

    /**
     * @return void
     */
    public function refresh()
    {
        $dir = ROOT_PATH . MIGRATIONS_PATH;
        $migrations_files = array();
        $migrations_db = array();
        $new_migrations = array();

        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if (($file == '.') || ($file == '..') || ($file == '...')) continue;

                    $migrations_files[] = str_ireplace('.migration.php', '', $file);
                };

                closedir($dh);
            }
        };

        $prepare = $this->dbo
            ->select(['name'])
            ->from('migrations')
            ->run();

        foreach ($prepare as $elem) {
            $migrations_db[] = $elem['name'];
        };

        $new_migrations = array_diff($migrations_files, $migrations_db);

        foreach ($new_migrations as $new_migration) {
            $this->insertNewMigrationIntoDB($new_migration, date('Y-m-d'), '');
        };
    }

    /**
     * @param $name
     * @return int
     */
    public function getMigrationPosition($migrationName)
    {
        return intval($migrationName[strlen($migrationName) - 2] . $migrationName[strlen($migrationName) - 1]);
    }

    /**
     * @param $migrationName
     * @param $created_at
     * @param $migrated_at
     * @param $comment
     * @return void
     */
    public function insertNewMigrationIntoDB($migrationName, $created_at, $migrated_at, $comment = '')
    {
        $position = $this->getMigrationPosition($migrationName);

        $this->dbo
            ->insert([
                'created_at'    => $created_at,
                'position'      => $position,
                'name'          => $migrationName,
                'comment'       => $comment
            ]);
    }
}