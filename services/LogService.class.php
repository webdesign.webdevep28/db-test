<?php

require_once __DIR__ . '/core/Singleton.pattern.php';
require_once __DIR__ . '/contracts/LogService.interface.php';

define ("DIR_ROOT_PATH", $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR);

/**
 * This class was created for debug some data into files /debug.log and /error.log
 */
class LogService extends Singleton implements LogServiceInterface
{
    protected static $debug_filename = 'debug.log';
    protected static $error_filename = 'error.log';

    /**
     * @param mixed $message
     * @param string $messageType: alert, critical, debug, emergency, error, info, log, notice, warning
     * @return void
     */
    public static function put($message, string $messageType, array $trace): void
    {
        if (is_array($message) || is_object($message)) $message = json_encode($message);

        if (!$trace || empty($trace)) {
            $trace = debug_backtrace();
        };

        $data = array(
            'file'      => str_replace(DIR_ROOT_PATH, '', $trace[0]['file']),
            'line'      => $trace[0]['line']
        );

        $output = date('H:i') . " ➤ {$data['file']} [" . strtoupper($messageType) . "] ({$data['line']}): {$message}\r\n";

        switch($messageType) {
            case 'emergency':
            case 'critical':
            case 'error':
                $filename = self::$error_filename;

                break;

            default:
                $filename = self::$debug_filename;
        };

        $f= fopen(DIR_ROOT_PATH . $filename, 'a+');

        fwrite($f, $output);

        fclose($f);
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function alert($message): void
    {
        static::put($message, 'alert', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function critical($message): void
    {
        static::put($message, 'critical', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function debug($message): void
    {
        static::put($message, 'debug', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function emergency($message): void
    {
        static::put($message, 'emergency', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function error($message): void
    {
        static::put($message, 'error', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function info($message): void
    {
        static::put($message, 'info', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function notice($message): void
    {
        static::put($message, 'notice', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function warning($message): void
    {
        static::put($message, 'warning', debug_backtrace());
    }
}
